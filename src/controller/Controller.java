//CONTROLLER

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.BankAccount;     //'model'package.classname'BankAccount'
import gui.InvestmentFrame;   //'gui'package.classname'InvestmentFrame'

public class Controller {
	
	
	private static final double INITIAL_BALANCE = 1000; //set initial balance at 1000
	//If set to zero, nothing changed (0 * anything = 0)
	
	class ListenerMgr implements ActionListener {

		@Override
		
		// this method calculate the balance
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//System.exit(0);
			
			double rate = Double.parseDouble(frame.getRate());
			double interest = account.getBalance()* rate / 100;
            account.deposit(interest);
			//resultLabel.setText("balance: " + account.getBalance()); 
            //frame.getLabel();
            frame.setResult(""+account.getBalance());

		}

	}
	
	//main method(this class run first)
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller();
	}

	public Controller() {
		frame = new InvestmentFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 100);	//set width, height of GUI box
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		
		/*
		card1.deposit(200);
		frame.setResult("Deposit : "+card1.getBalance()+"Baht");
		card1.getBalance();
		frame.extendResult("CheckMoney : "+card1.getBalance()+"Baht");
		card1.withdraw(100);
		frame.extendResult("Withdraw : "+card1.getBalance()+"Baht");
		card1.getBalance();
		frame.extendResult("CheckMoney : "+card1.toString()+"Baht");
		*/
	}
	double rate;
	private BankAccount account = new BankAccount(INITIAL_BALANCE);  //create new variable 'account'/account is attribute
																	 //because it can update balance 
	ActionListener list;
	InvestmentFrame frame;	//create new variable 'frame', account is attribute because it can be use in class Controller's method
}